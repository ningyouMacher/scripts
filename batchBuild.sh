#!/usr/bin/bash

NAME="$(dirname $PWD)"

for GEO in Headers/Geometry/*.tex; do
	X="${GEO##*/}"
	cat $GEO main.tex| pdflatex -jobname="${NAME##*/}.${X%.tex}"
	cat $GEO main.tex| pdflatex -jobname="${NAME##*/}.${X%.tex}"
	cat $GEO main.tex| pdflatex -jobname="${NAME##*/}.${X%.tex}"
done

rm *.toc *.aux *.log *.out
