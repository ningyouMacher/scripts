#!/bin/bash

git filter-branch -f --prune-empty --tag-name-filter cat --tree-filter "\
	if [ -f $1 ]; then
		mv $1 $2
	fi
	" -- --all
