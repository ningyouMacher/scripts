#!/bin/bash

git filter-branch -f --prune-empty --tag-name-filter cat --tree-filter "rm -f $1" -- --all
git for-each-ref --format="%(refname)" refs/original/ | xargs -n 1 git update-ref -d
