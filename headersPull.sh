#!/usr/bin/bash

Name=${PWD##*/}
# Note that this will not work with MttM until I change the directory name to something without the ".new" or fix the script to accomadate, but that is a problem for later.

MESSAGE=`git log --format=%B -n 1 Headers/$Name`
HASH=`git log --format=%H -n 1 Headers/$Name`

git fetch Headers
for FILE in $(git ls-tree -r Headers/$Name|cut -f2); do
	git show Headers/$Name:$FILE > Headers/$FILE
done

git add Headers

printf "Checked out from $HASH\nFrom: Header's submodule\n$MESSAGE" | git commit -F-
