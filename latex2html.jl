#!/usr/bin/julia -O0

# __precompile__() # The goggles! They do nothing!
# Actually, precompiling slows this down?

module LaTeX2HTML

using Dates
using Pipe

function keybase_header(title::String,author::String="Ningyou Macher")
	return """
	<!DOCTYPE html>
	<html xmlns="http://www.w3.org/1999/xhtml" lang="" xml:lang="">
	<head>
		<meta charset="utf-8" />
		<meta name="generator" content="pandoc" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes" />
		<meta name="author" content="$author" />
		<title>$title</title>
		<style>
			code{white-space: pre-wrap;}
			span.smallcaps{font-variant: small-caps;}
			span.underline{text-decoration: underline;}
			div.column{display: inline-block; vertical-align: top; width: 50%;}
			div.hanging-indent{margin-left: 1.5em; text-indent: -1.5em;}
			ul.task-list{list-style: none;}
		</style>
		<link rel="stylesheet" href="https://ningyoumacher.keybase.pub/pandoc.css" />
		<!--[if lt IE 9]>
			<script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv-printshiv.min.js"></script>
		<![endif]-->
	</head>
	<body>
	<header id="title-block-header">
	<h1 class="title">$title</h1>
	<p class="author">$author</p>
	</header>
	"""
end

function keybase_footer(starting_year::String,author::String = "Ningyou Macher")
	return """
	<hr>
	<p>&copy;$(starting_year)–$(Dates.year(now())) $author
	</body>
	</html>
	"""
end

function gromets_header(title::String,ch_preface::SubString,base_name::String,tags::String,commit,author::String="Ningyou Macher")

	ch_header, ch_prologue = split(ch_preface,r"<h1.+?\K</h1>\n?",limit=2)
	ch_title = replace(ch_header,r"<h1.+?>(.+)" => s"\1")

	if !isempty(ch_prologue)
		ch_prologue = replace(ch_prologue,r"\n\s*?<hr><br><hr>" => s"")
		ch_prologue = """

		<div id='prologue' class="story5l">
		$ch_prologue
		</div>
		"""
	end

	return """
	<!-- built from $commit on $(Dates.today()) -->
	<!-- $title $author $tags -->
	$ch_prologue
	<!-- story goes here -->
	<div id='main' class="storym">
		<h3>Chapter $base_name - $ch_title</h3>
	"""

end

function gromets_footer(ch_epilogue::SubString)


	if !isempty(ch_epilogue)
		ch_epilogue_header, ch_epilogue = split(ch_epilogue,r"<h3.*?>.+?</h3>\K\n\t?")
		ch_epilogue_header = replace(ch_epilogue_header,r"<h3.*?>(.+)</h3>" => s"\1")
		ch_epilogue = """

		<div id='epilogue' class="story5l">
			<p style="font-size: large;">$ch_epilogue_header</p>
			$ch_epilogue</div>
		"""
	end


	return """
	</div>

	<!-- end story -->


	$ch_epilogue"""

end

function deMacro(file::String)
	io = PipeBuffer()
	line = read(file,String)

	find_replace = [
		r"\\dinkus" => s"LATEXDINKUS"
		r"\\lb" => s"\n\nLATEXLB"
	]
	for rs in find_replace
		line = replace(line,rs)
	end

	print(io,line)

	return io

end

function reMacro(cmd::Base.AbstractCmd)
# function reMacro(args::Tuple{Base.AbstractCmd,String})

	# cmd = args[1]
	# base_name = args[2]

	line = read(cmd,String)

	find_replace = [ # Need to reverse the subs... just in case
		r"<p>LATEXLB<\/p>" => s"<hr><br><hr>"	# Need both just in case
		r"LATEXLB<\/p>" => s"<hr><br><hr>"		# Also, this one needs to be second
		r"<p>LATEXDINKUS<\/p>" => s"<hr>"
		r"\n\K" => s"\t"
	]
	for rs in find_replace
		line = replace(line,rs)
	end

	rs = r"<p>(.?)<span class=\"smallcaps\">" =>
		 s"<p class=\"notab\">\1<span style=\"font-variant: small-caps\">"
	line = replace(line,rs,count=1)


	return line[1:end-1]

end

function post_processing(raw_html::String,base_name)
# function post_processing(args::Tuple{String,String})
#
#	raw_html = args[1]
#	base_name = args[2]
	out_file = base_name*".html"
	write("HTML/Raw/"*out_file,raw_html)

	title="Married to the Maid"
	# Really should put this shit in some kind of data structure...
	if occursin(r"^0$",base_name)
		author="Ningyou Macher"
		cr_author="Ningyou Macher"
		tags="cons; X"
	elseif occursin(r"^1$",base_name)
		author="PoseMe, edited by Ningyou Macher"
		cr_author="PoseMe &amp; Ningyou Macher"
		tags="Solo-F; machine/f; robots; maids; collar; switch; mc; Fmachine/f; latex; heels; program; pod; recharge; technician; reprogram; reboot; discovery; M/f; stuck; f2robotmaid; cons; X"
	elseif occursin(r"^2$",base_name)
		author="FembotFan, edited by Ningyou Macher"
		cr_author="PoseMe, FembotFan &amp; Ningyou Macher"
		tags="Solo-F; Machine/f; robots; maid-bots; collar; switch; mind-control; latex; program; pod; recharge; F/f; discovery; torment; bdsm; spank; FF; technician; bodymod; cyborg; stuck; F2maidbot; cons(I'm not sure about this tag); X"
	elseif occursin(r"^3$",base_name)
		author="FembotFan, edited by Ningyou Macher"
		cr_author="FembotFan &amp; Ningyou Macher"
		tags="Solo-F; Machine/f; robots; maid-bots; collar; switch; mind-control; latex; program; FF; technician; discovery; bodymod; cyborg; F2maidbot; F/maidbot; toys; lesbian; sex; pod; recharge; stuck; cons; X (not sure of this tag)"
	elseif occursin(r"^4$",base_name)
		author="Ningyou Macher"
		cr_author="Ningyou Macher"
		tags="Solo-F; Machine/f; robots; maid-bots; collar; switch; mind-control; latex; program; technician; bodymod; cyborg; F2maidbot; F/maidbot; toys; sex; pod; recharge; stuck; nc; XX"
	else
		author="Ningyou Macher"
		cr_author="Ningyou Macher"
		tags=""
	end
	# Authors = Dict[
	# 			"0" => "Ningyou Macher",
	# 			"1" => "PoseMe, edited by Ningyou Macher",
	# 			"2" => "FembotFan, edited by Ningyou Macher",
	# 			"3" => "FembotFan, edited by Ningyou Macher",
	# 			"4" => "Ningyou Macher"
	# ]


	commit = chomp(read(`git rev-parse --verify HEAD`,String))

	ch_preface, ch_body = split(raw_html,r"\n*?(?=\t<p class=\"notab\">)",limit=2)
	ch_body_split = split(ch_body,r"\n+?\t?(?=<h[0-9].+?</h[0-9]>)",limit=2)
	ch_body = ch_body_split[1]
	if isassigned(ch_body_split,2)
		ch_epilogue = ch_body_split[2]
	else
		ch_epilogue = SubString("")
	end

	write("HTML/Keybase/"*out_file,keybase_header(title,author)*raw_html*keybase_footer("2017",cr_author))

	write("HTML/Gromets/"*out_file,gromets_header(title,ch_preface,base_name,tags,commit)*ch_body*gromets_footer(ch_epilogue))

end

# Strictly speaking, this function shouldn't assume the input or output directories.
# Maybe something to check if a file is empty?
function latex2html(list::Array{String})

	if !isdir("HTML/Raw")
		mkpath("HTML/Raw")
	end
	if !isdir("HTML/Keybase")
		mkdir("HTML/Keybase")
	end
	if !isdir("HTML/Gromets")
		mkdir("HTML/Gromets")
	end

	TITLE="Married to the Maid"
	pandoc = `pandoc --from latex --to html`

	Threads.@threads for file in list

		base_name = splitext(file)[1]
		in_file = "Chapters/"*base_name*".tex"

		# currently skipping some troublesome files.
		# later do something else with them.
		if occursin(r"[0-9]+\.",file)
			# Perhaps try @Pipe.pipe?
			# Need another function, probably one that reads from a file...
			# Pipe.@pipe raw_html = pipeline(deMacro(in_file),pandoc) |> reMacro(_,base_name)
			# @pipe raw_html = pipeline(deMacro(in_file),pandoc) |> reMacro(_,base_name)
			# ^ that one was better, but still no dice.
			# raw_html =
			# (pipeline(deMacro(in_file),pandoc) |> reMacro ,base_name) |> post_processing
			# println(@pipe 1 |> +(_,1))
			# ^ That works!
			@pipe pipeline(deMacro(in_file),pandoc) |> reMacro |> post_processing(_,base_name)
			# ^ This now works, but it's slower than the original one by a bit more than 10%.
			# It looks nicer, so it stays. I mean, it still is faster than the old script despite doing so much more.
		end
	end
end

# # I think these might actually make it faster.
# precompile(deMacro,(String,))
# precompile(reMacro,(Tuple{Base.AbstractCmd,String},))
# precompile(latex2html,(Array{String},))
# # At the least, they don't seem to make it slower.
# Okay, now they are making it slower.


end # This closes the module

git_isclean = try
	read(`git diff-index --quiet HEAD --`)
	true
catch
	false
end

# # For testing purposes...
# git_isclean = true


if isdir("Chapters") && !isempty(readdir("Chapters")) && git_isclean
	file_list = filter(x->occursin(r".tex$",x), readdir("Chapters"))
	LaTeX2HTML.latex2html(file_list)
elseif !git_isclean
	println("Please commit changes in Git")
elseif isdir("Chapters")
	println(pwd()*"/Chapters is empty")
elseif isempty(readdir("Chapters"))
	println("Chapters is not a subdirectory of ",pwd())
else
	# I don't think PROGRAM_FILE does anything.
	# Either way this should never happen
	println("Not sure what happened\nPlease examine ",@__FILE__)
end

# const TMP = mktempdir()*"/"

# for file in file_list
# 	base_name = splitext(file)[1]
# 	deMacro("Chapters/"*file,TMP*base_name*".tex")
# 	pandoc(TMP*base_name*".tex",TMP*base_name*".html")
# 	reMacro(TMP*base_name*".html","HTML/"*base_name*".html")
# end
