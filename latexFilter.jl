#!/usr/bin/julia

__precompile__()

module latexFilter

function cleanTex(fileIn::String,fileOut=""::String)
	input = open(fileIn)
	if  fileOut == ""
		output = stdout
	else
		output = open(fileOut)
	end
	cleanTexLoop(input,output)
	close(input)
	if output != stdout
		close(output)
	end
end

function smudgeTexLoop(input=stdin::IO,output=stdout::IO)
	write(output,newLineFilter(
			read(input,String)
	))
end

function cleanTexLoop(input=stdin::IO,output=stdout::IO) # This is what gitLatex.clean runs
	for line = eachline(input)
		applyFilters(line,output)
	end
end

function applyFilters(line::String,output::IO)
	if isCommentLine(line)
 	elseif ! hasNewLine(line) # needed incase smudge fails
		indent = captureIndent(line)
		splitLine = splitComments(line)
		par = splitLine[1]
		par = sentenceFilter(par,indent)
		if length(splitLine) == 2
			com = splitLine[2]
	 		line = par * "%CNL%\n" * indent * "%" * com
	 		# line = par * "\n%CNL%\n" * indent * "%" * com
		else
			line = par
		end
	end
	write(output,line::String*"\n")
end

function captureIndent(line::String)
	return match(Base.Regex("^\\h*"),line).match
end

function splitComments(line::String)
	regX = "^.+?[^\\\\]\\K%"
	return split(line,Base.Regex(regX))
end

function sentenceFilter(sstr::SubString,indent::SubString)
	regX = "(?:(?:\\.|\\?|!|;|:|(?:ldots|--) *(?:\"|''))'?(?:\"|{?''}?)? +(?=\\S)\\K|\\w+---\\K(?=\\w+))"
	subs = "%NL%\n" * indent # No space at the end
	# subs = "\n%NL%\n" * indent # For next time I feel like changing it
	return applySub(sstr,regX,subs)
end

function newLineFilter(line::String)
	regX = "%C?NL%\n\\h*"
	# regX = "\n%C?NL%\n\\h*"
	subs = ""
	return applySub(line,regX,subs)
end

function hasNewLine(line::String)
	return occursin(Base.Regex("%C?NL%"),line)
end

function isCommentLine(line::String)
	return occursin(r"^\h*%",line)
end

function applySub(line::AbstractString,regX::String,sub::String)
	return replace(
		line,
		Base.Regex(regX) =>
		Base.SubstitutionString(sub)
	)
end

# https://github.com/gilesbowkett/git-smudge-and-clean

end # module
