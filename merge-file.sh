#!/usr/bin/bash

if [ ! -d /tmp/git-merge-file ]; then
	mkdir /tmp/git-merge-file
fi

git show editing/stage1:Chapters/$1.tex > /tmp/git-merge-file/stage1
git show writing:Chapters/$1.tex > /tmp/git-merge-file/writing
git show editing/stage2:Chapters/$1.tex > /tmp/git-merge-file/stage2

git merge-file --stdout --ours -L "editing/stage1" -L "writing" -L "editing/stage2" /tmp/git-merge-file/{stage1,writing,stage2} > $1.ours.tex
git merge-file --stdout --theirs -L "editing/stage1" -L "writing" -L "editing/stage2" /tmp/git-merge-file/{stage1,writing,stage2} > $1.theirs.tex

vimdiff $1.ours.tex $1.theirs.tex
