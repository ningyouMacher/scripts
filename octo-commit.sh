#!/bin/bash

if [ "$1" == "-h" -o "$1" == "" ]; then
	echo "branchA branchB file [message]"
	exit 0
fi

if [ "$4" == "" ]; then
	MSG="octo-commit $@"
else
	MSG="$4"
fi

git add $3

parentA=`git log --format='%H' --follow  $1 $3|head -n1`
parentB=`git log --format='%H' --follow  $2 $3|head -n1`
tree=`git write-tree`
commit=`echo "$MSG" | git commit-tree -p HEAD -p $parentA -p $parentB $tree`

printf "\n"
echo "First parent is $parentA"
echo "Second parent is $parentB"
echo "Tree is $tree"
echo "Commit is $commit"
printf "\n"

git update-ref refs/heads/`git rev-parse --abbrev-ref HEAD` $commit
