#!/usr/bin/bash

MESSAGE=`git log --format=%B -n 1 $1`
HASH=`git log --format=%H -n 1 $1`

for FILE in ${@:2}; do
	git checkout $1 $FILE
done

printf "Checked out from $HASH\nFrom: $1\n$MESSAGE" | git commit -F-
