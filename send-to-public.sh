#!/usr/bin/bash

branch=`git rev-parse --abbrev-ref HEAD`
commit=`git rev-list --max-count=1 HEAD`
messge=`git log --format=%B -n 1 HEAD|head -n1`

printf "%% From $branch\n" > .Public/$1
printf "%% $commit\n" >> .Public/$1
printf "%% $messge\n" >> .Public/$1

cat $1 >> .Public/$1
