#!/usr/bin/bash

NAME="$(dirname $PWD)"

cat $1 main.tex| pdflatex -jobname="${NAME##*/}.test"
# cat $1 main.tex| pdflatex -jobname="${NAME##*/}.test"
# cat $1 main.tex| pdflatex -jobname="${NAME##*/}.test"

rm *.toc *.aux *.log *.out
