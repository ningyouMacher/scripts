#!/usr/bin/bash

cat "$1"|gitLatex.clean > /tmp/Gtool.local
cat "$2"|gitLatex.clean > /tmp/Gtool.base
cat "$3"|gitLatex.clean > /tmp/Gtool.remote

git merge-file --stdout --ours /tmp/Gtool.{local,base,remote} > /tmp/Gtool.ours
git merge-file --stdout --theirs /tmp/Gtool.{local,base,remote} > /tmp/Gtool.theirs

"$5" /tmp/Gtool.ours /tmp/Gtool.theirs #  --output /tmp/Gtool.out

cat /tmp/Gtool.theirs |gitLatex.dirty > "$4"
